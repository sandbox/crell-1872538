<?php

/**
 * @file
 * Contains Drupal\eight\EightBundle.
 */

namespace Drupal\eight;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Bundle to register services on behalf of this module.
 */
class EightBundle extends Bundle {

  /**
   * Overrides Bundle::build().
   */
  public function build(ContainerBuilder $container) {
    // Put your container registration code here.  It's just direct calls against
    // the container builder object.  You can also register compiler passes.
  }
}

