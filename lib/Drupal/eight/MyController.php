<?php

namespace Drupal\eight;

use Symfony\Component\DependencyInjection\ContainerAware;

/**
 * A controller for my things.
 */
class MyController extends ContainerAware {

  /**
   * Does something.
   */
  public function something($b, $a) {

  }

}
